#!/bin/bash
mkdir /tmp/gnunet_gstreamer/
mkfifo /tmp/gg_fifo
export GST_DEBUG_DUMP_DOT_DIR=/tmp/gnunet_gstreamer 
export GST_DEBUG_FILE=/tmp/gnunet_gstreamer/gstreamer_gnunet_record.log
export GST_DEBUG=4
#./gnunet-helper-audio-record-experimental | GST_DEBUG_FILE=/tmp/gnunet_gstreamer/gstreamer_gnunet_playback.log ./gnunet-helper-audio-playback-experimental
./gnunet-helper-audio-record-experimental > /tmp/ggfifo 2> /tmp/gnunet_gstreamer/record_stderr.log &
REC_PID=$!
export GST_DEBUG_FILE=/tmp/gnunet_gstreamer/gstreamer_gnunet_playback.log
gdb --args  ./gnunet-helper-audio-playback-experimental 

kill -15 $REC_PID
kill -9 $REC_PID
pkill -P $$

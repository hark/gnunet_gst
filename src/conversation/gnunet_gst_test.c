#include "gnunet_gst_def.h"
#include "gnunet_gst.h"

//enable debugging
#define GGD


  int
main (int argc, char *argv[])
{
  struct GNUNET_gstData *gst;
  GstBus *bus;
  GstMessage *msg;
  GstElement *gnunetsrc, *gnunetsink, *source, *sink, *encoder, *decoder;

  gst = (GNUNET_gstData*)malloc(sizeof(struct GNUNET_gstData));

  gg_load_configuration(gst);

  /* Initialize GStreamer */
  gst_init (&argc, &argv);

  gst->pipeline = GST_PIPELINE(gst_pipeline_new ("gnunet-media-helper"));

#ifdef IS_SPEAKER
  int type = SPEAKER;

#ifdef GGD
  fprintf(stderr,"this is the speaker \n");
#endif

#endif
#ifdef IS_MIC
  int type = MICROPHONE;
#ifdef GGD
  fprintf(stderr,"this is the microphone \n");
#endif
#endif
  if ( type == SPEAKER)
  {

    gnunetsrc = GST_ELEMENT(get_app(gst, SOURCE));  

    sink = GST_ELEMENT(get_audiobin(gst, SINK));
    decoder = GST_ELEMENT(get_coder(gst, DECODER));
    gst_bin_add_many( GST_BIN(gst->pipeline), gnunetsrc, decoder, sink, NULL);
    gst_element_link_many( gnunetsrc, decoder, sink , NULL);

  } 
  if ( type == MICROPHONE ) {

    source = GST_ELEMENT(get_audiobin(gst, SOURCE));

    encoder = GST_ELEMENT(get_coder(gst, ENCODER));

    gnunetsink = GST_ELEMENT(get_app(gst, SINK));  

    gst_bin_add_many( GST_BIN(gst->pipeline), source, encoder, gnunetsink, NULL);
    gst_element_link_many( source, encoder, gnunetsink , NULL);

  }

  pl_graph(gst->pipeline); 

  /* Start playing */
  gst_element_set_state (GST_ELEMENT(gst->pipeline), GST_STATE_PLAYING);

  /* Wait until error or EOS */
  //bus = gst_element_get_bus (GST_ELEMENT(gst->pipeline));
  //bus_watch_id = gst_bus_add_watch (bus, gnunet_gst_bus_call, pipeline);

  gg_setup_gst_bus(gst);

  // start pushing buffers
  if ( type == MICROPHONE )
  {

    GMainLoop *loop;
    loop = g_main_loop_new (NULL, FALSE);

    g_main_loop_run (loop);

  } 
  if ( type == SPEAKER )
  {
    while ( 1 )
    {
      gnunet_read(gst);
    }
  }

  gst_object_unref (bus);
  gst_element_set_state (GST_ELEMENT(gst->pipeline), GST_STATE_NULL);
  gst_object_unref (gst->pipeline);

  return 0;
}

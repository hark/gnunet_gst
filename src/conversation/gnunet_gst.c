#include "gnunet_gst_def.h"
#include <stdlib.h>

//enable debugging
#define GGD

/**
 * Our configuration.
 */
static struct GNUNET_CONFIGURATION_Handle *cfg;

  void
dump_buffer(unsigned n, const unsigned char* buf)
{
  const unsigned char *p, *end;
  unsigned i, j;

  end = buf + n;

  for (i = 0; ; i += 16) {
    p = buf + i;
    for (j = 0; j < 16; j++) {
      fprintf(stderr, "%02X ", p[j]);
      if (p + j >= end)
        goto BREAKOUT;
    }
    fprintf(stderr, " ");
    p = buf + i;
    for (j = 0; j < 16; j++) {
      fprintf(stderr, "%c", isprint(p[j]) ? p[j] :
          '.');
      if (p + j >= end)
        goto BREAKOUT;
    }
    fprintf(stderr, "\n");
  }
BREAKOUT:
  return;
}

/***
 * load gnunet configuration
 */
  void
gg_load_configuration(GNUNET_gstData * d)
{
  char *audiobackend_string;
  cfg =  GNUNET_CONFIGURATION_create();
  GNUNET_CONFIGURATION_load(cfg, "mediahelper.conf");

  char *section = "MEDIAHELPER";

  GNUNET_CONFIGURATION_get_value_string(cfg, "MEDIAHELPER", "JACK_PP_IN", &d->jack_pp_in);
  GNUNET_CONFIGURATION_get_value_string(cfg, "MEDIAHELPER", "JACK_PP_OUT", &d->jack_pp_out);

  GNUNET_CONFIGURATION_get_value_string(cfg, "MEDIAHELPER", "AUDIOBACKEND", &audiobackend_string);

  // printf("abstring: %s \n", audiobackend_string);

  if ( audiobackend_string == "AUTO" )
  {
    d->audiobackend = AUTO;
  } else if ( audiobackend_string = "JACK" )
  {
    d->audiobackend = JACK;
  } else if ( audiobackend_string = "ALSA" )
  {
    d->audiobackend = ALSA;
  } else if ( audiobackend_string = "FAKE" )
  {
    d->audiobackend = FAKE;
  } else if ( audiobackend_string = "TEST" )
  {
    d->audiobackend = TEST;
  } else 
  {
    d->audiobackend = AUTO;
  }

  if (GNUNET_CONFIGURATION_get_value_yesno(cfg, "MEDIAHELPER", "REMOVESILENCE") == GNUNET_YES)
  {
    d->dropsilence = TRUE;
  } else {
    d->dropsilence = FALSE;
  }

  if (GNUNET_CONFIGURATION_get_value_yesno(cfg, "MEDIAHELPER", "NO_GN_HEADERS") == GNUNET_YES)
  {
    d->pure_ogg = TRUE;
  } else {
    d->pure_ogg = FALSE;
  }


  if (GNUNET_CONFIGURATION_get_value_yesno(cfg, "MEDIAHELPER", "USERTP") == GNUNET_YES)
  {
    d->usertp = TRUE;
  } else {
    d->usertp = FALSE;
  }

  //  GNUNET_CONFIGURATION_write(cfg, "mediahelper.conf");

}

  static void
write_data (const char *ptr, size_t msg_size)
{
  ssize_t ret;
  size_t off;
  off = 0;
  while (off < msg_size)
  {
    ret = write (1, &ptr[off], msg_size - off);
    if (0 >= ret)
    { 
      if (-1 == ret)
        GNUNET_log_strerror (GNUNET_ERROR_TYPE_ERROR, "write");
      exit (2);
    }
    off += ret;
  }
}



  extern GstFlowReturn
on_appsink_new_sample (GstElement * element, GNUNET_gstData * d)
{
  static unsigned long long toff;

  //size of message including gnunet header
  size_t msg_size;

  GstSample *s;
  GstBuffer *b;
  GstMapInfo map;
  /*
     const GstStructure *si;
     char *si_str;
     */
  GstCaps *s_caps;
  char *caps_str;

  if (gst_app_sink_is_eos(GST_APP_SINK(element)))
    return GST_FLOW_OK;

  //pull sample from appsink
  s = gst_app_sink_pull_sample (GST_APP_SINK(element));

  if (s == NULL)
    return GST_FLOW_OK;

  if (!GST_IS_SAMPLE (s))
    return GST_FLOW_OK;

  s_caps = gst_sample_get_caps(s);
  caps_str = gst_caps_to_string(s_caps);
#ifdef GGD
  fprintf(stderr, "caps: %s \n ", caps_str);
#endif
  b = gst_sample_get_buffer(s);

  GST_WARNING ("caps are %" GST_PTR_FORMAT, gst_sample_get_caps(s));

  gst_buffer_map (b, &map, GST_MAP_READ);   

  size_t len;
  len = map.size;
#ifdef GGD
  fprintf(stderr, "length: %u \n", len);
#endif
  if (len > UINT16_MAX - sizeof (struct AudioMessage))
  { 
    // this should never happen?
#ifdef GGD
    fprintf(stderr,"sample from gstreamer too big!, exiting \n");
    exit(20);
#endif
    len = UINT16_MAX - sizeof (struct AudioMessage);
  }

  msg_size = sizeof (struct AudioMessage) + len;

  // copy the data into audio_message
  memcpy (((char *) &(d->audio_message)[1]), map.data, len);
  //printf("ab: type: %u size: %u \n", ntohs(d->audio_message->header.type), d->audio_message->header.size);
  //dump_buffer(sizeof (struct AudioMessage), &d->audio_message); 
  // FIXME
  (d->audio_message)->header.size = htons ((uint16_t) msg_size);
  (d->audio_message)->header.type = htons ((uint16_t) 730);


  toff += msg_size;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
      "Sending %u bytes of audio data (total: %llu)\n",
      (unsigned int) msg_size,
      toff);
  // printf("start of buf \n");
  //dump_buffer(msg_size, d->audio_message); 
  //write_data ((const char *) d->audio_message, msg_size);

  if (d->pure_ogg == TRUE)
  {
    // write the audio_message without the gnunet headers
#ifdef GGD    
    fprintf(stderr, "\n pure_ogg \n \n");
#endif
    //    write_data ((const char *) &(d->audio_message)[1], len);
  }
  else if (d->pure_ogg == FALSE)
  {
#ifdef GGD
    fprintf(stderr, "\n sending with gnunet headers \n \n");
#endif
    write_data ((const char *) d->audio_message, msg_size);
  }
  gst_sample_unref(s);
  return GST_FLOW_OK;
}

/***
 * Dump a pipeline graph
 */
  extern void
pl_graph(GstElement * pipeline)
{

#ifdef IS_SPEAKER
  //  gst_debug_bin_to_dot_file_with_ts(GST_BIN(pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "playback_helper.dot");
#endif
#ifdef IS_MIC
  //  gst_debug_bin_to_dot_file_with_ts(GST_BIN(pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "record_helper.dot");
#endif
}

  extern gboolean
gnunet_gst_bus_call (GstBus *bus, GstMessage *msg, gpointer data)
{
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG, 
      "Bus message\n");
  switch (GST_MESSAGE_TYPE (msg))
  {
    case GST_MESSAGE_EOS:
      GNUNET_log (GNUNET_ERROR_TYPE_INFO,
          "End of stream\n");
      exit (10);
      break;

    case GST_MESSAGE_ERROR:
      {
        gchar  *debug;
        GError *error;

        gst_message_parse_error (msg, &error, &debug);
        g_free (debug);

        GNUNET_log (GNUNET_ERROR_TYPE_ERROR, 
            "Error: %s\n", 
            error->message);
        g_error_free (error);

        exit (10);
        break;
      }
    default:
      break;
  }

  return TRUE;
}

/* called when pipeline changes state */
  extern void
state_changed_cb (GstBus * bus, GstMessage * msg, GNUNET_gstData * d)
{
  GstState old_state, new_state, pending_state;

  gst_message_parse_state_changed (msg, &old_state, &new_state,
      &pending_state);
  switch (new_state)
  {

    case GST_STATE_READY:
      //pl_graph(GST_ELEMENT(d->pipeline));
      break;
    case GST_STATE_PLAYING:

      //GST_LOG ("caps are %" GST_PTR_FORMAT, caps);

      pl_graph(GST_ELEMENT(d->pipeline));
      break;
    case GST_STATE_VOID_PENDING:
      //pl_graph(GST_ELEMENT(d->pipeline));
      break;
    case GST_STATE_NULL:
      //pl_graph(GST_ELEMENT(d->pipeline));
      break;

    case GST_STATE_PAUSED:
      //pl_graph(GST_ELEMENT(d->pipeline));
      break;
  }
}

  static void
application_cb (GstBus * bus, GstMessage * msg, GNUNET_gstData * data)
{
  return;
}

  static void
error_cb (GstBus * bus, GstMessage * msg, GNUNET_gstData * data)
{
  return;
}

  static void
eos_cb (GstBus * bus, GstMessage * msg, GNUNET_gstData * data)
{
  return;
}

  extern void
gg_setup_gst_bus (GNUNET_gstData * d)
{
  GstBus *bus;
  bus = gst_element_get_bus (GST_ELEMENT(d->pipeline));
  gst_bus_add_signal_watch (bus);
  g_signal_connect (G_OBJECT (bus), "message::error", (GCallback) error_cb,
      d);
  g_signal_connect (G_OBJECT (bus), "message::eos", (GCallback) eos_cb,
      d);
  g_signal_connect (G_OBJECT (bus), "message::state-changed",
      (GCallback) state_changed_cb, d);
  g_signal_connect (G_OBJECT (bus), "message::application",
      (GCallback) application_cb, d);
  g_signal_connect (G_OBJECT (bus), "message::about-to-finish",
      (GCallback) application_cb, d);
  gst_object_unref (bus);

}
  extern int
feed_buffer_to_gst (const char *audio, size_t b_len, GNUNET_gstData * d)
{
  GstBuffer *b;
  gchar *bufspace;
  GstFlowReturn flow;

  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
      "Feeding %u bytes to GStreamer\n",
      (unsigned int) b_len);

  bufspace = g_memdup (audio, b_len);
  b = gst_buffer_new_wrapped (bufspace, b_len);
  if (NULL == b)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
        "Failed to wrap a buffer\n");
    g_free (bufspace);
    return GNUNET_SYSERR;
  }
  if (GST_APP_SRC(d->appsrc) == NULL)
    exit(10);
  flow = gst_app_src_push_buffer (GST_APP_SRC(d->appsrc), b);
  /* They all return GNUNET_OK, because currently player stops when
   * data stops coming. This might need to be changed for the player
   * to also stop when pipeline breaks.
   */
  switch (flow)
  {
    case GST_FLOW_OK:
      GNUNET_log (GNUNET_ERROR_TYPE_DEBUG, 
          "Fed %u bytes to the pipeline\n",
          (unsigned int) b_len);
      break;
    case GST_FLOW_FLUSHING:
      /* buffer was dropped, because pipeline state is not PAUSED or PLAYING */
      GNUNET_log (GNUNET_ERROR_TYPE_INFO,
          "Dropped a buffer\n");
      break;
    case GST_FLOW_EOS:
      /* end of stream */
      GNUNET_log (GNUNET_ERROR_TYPE_INFO, 
          "EOS\n");
      break;
    default:
      GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
          "Unexpected push result\n");
      break;
  }
  return GNUNET_OK;
}

/**
 * debug making elements
 */
  extern GstElement *
gst_element_factory_make_debug( gchar *factoryname, gchar *name)
{
  GstElement *element;

  element = gst_element_factory_make(factoryname,name);

  if (element == NULL) {

    fprintf (stderr,"\n Failed to create element - type: %s name: %s \n", factoryname, name);
    exit(10);
    return element;
  } else {
    return element;
  }
}

  extern void
lf(char * msg)
{
  fprintf(stderr,"linking elements failed: %s", msg);
  exit(10);
}

/***
 * used to set properties on autoaudiosink's chosen sink
 */
  static void
autoaudiosink_child_added (GstChildProxy *child_proxy,
    GObject *object, 
    gchar *name,
    gpointer user_data)
{
  if (GST_IS_AUDIO_BASE_SRC (object))
    g_object_set (object,
        "buffer-time", (gint64) BUFFER_TIME, 
        "latency-time", (gint64) LATENCY_TIME,
        NULL);
}

/***
 * used to set properties on autoaudiosource's chosen sink
 */
  static  void
autoaudiosource_child_added (GstChildProxy *child_proxy, GObject *object, gchar *name, gpointer user_data)
{
  if (GST_IS_AUDIO_BASE_SRC (object))
    g_object_set (object, "buffer-time", (gint64) BUFFER_TIME, "latency-time", (gint64) LATENCY_TIME, NULL);
}

  GstPipeline *
get_pipeline(GstElement *element)
{
  GstPipeline *p;

  p = GST_PIPELINE(gst_object_get_parent(GST_OBJECT(element)));

  return p;
}

  static void
decoder_ogg_pad_added (GstElement *element, 
    GstPad *pad,
    gpointer data)
{
  GstPad *sinkpad;
  GstElement *decoder = (GstElement *) data;
#ifdef GGD
  fprintf(stderr,"==== ogg pad added callback \n");
#endif
  /* We can now link this pad with the opus-decoder sink pad */
  //  pl_graph(get_pipeline(element));
  sinkpad = gst_element_get_static_pad (decoder, "sink");

  gst_pad_link (pad, sinkpad);
  gst_element_link_many(element, decoder, NULL);
  gst_object_unref (sinkpad);
}

  int
gnunet_read (GNUNET_gstData * d) 
{
  char readbuf[MAXLINE];
  int ret;
  ret = read (0, readbuf, sizeof (readbuf));
  if (0 > ret)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
        _("Read error from STDIN: %d %s\n"),
        ret, strerror (errno));
#ifdef GGD
    fprintf(stderr, "read error from STDIN");
#endif
    return FAIL;
  }
  //toff += ret;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
      "Received %d bytes of audio data\n",
      (int) ret);
  //  fprintf(stderr,"Received %d bytes of audio data\n",
  //      (int) ret);

  if (0 == ret)
    return FAIL;

  if (d->pure_ogg)
  {
#ifdef GGD
    fprintf(stderr, "pure_ogg receiver \n");
#endif
    feed_buffer_to_gst (readbuf, ret, d);
  }
  else
  { 
#ifdef GGD
    fprintf(stderr,"mst receiver\n");
#endif
    switch( GNUNET_SERVER_mst_receive (d->stdin_mst, NULL,      readbuf, ret,      GNUNET_NO, GNUNET_NO)) 
    {
      case GNUNET_OK:
#ifdef GGD
        fprintf(stderr, "OK \n");
#endif
        break;
      case GNUNET_NO:
#ifdef GGD
        fprintf(stderr, "NO \n");
#endif
        break;
      case GNUNET_SYSERR:
#ifdef GGD
        fprintf(stderr, "SYSERR \n");
#endif
        break;
    }

  }
}

/**
 * Message callback
 */
  static int
stdin_receiver (void *cls,
    void *client,
    const struct GNUNET_MessageHeader *msg)
{
  struct AudioMessage *audio;
  size_t b_len;
#ifdef GGD
  fprintf(stderr,"stdin receiver \n ");
  //dump_buffer(sizeof(msg), msg);
#endif
  switch (ntohs (msg->type))
  {
    case GNUNET_MESSAGE_TYPE_CONVERSATION_AUDIO:
      audio = (struct AudioMessage *) msg;

      b_len = ntohs (audio->header.size) - sizeof (struct AudioMessage);
#ifdef GGD
      fprintf(stderr,"feeding buffer to gst \n ");
#endif
      feed_buffer_to_gst ((const char *) &audio[1], b_len, cls);
      break;
    default:
#ifdef GGD
      fprintf(stderr,"No audio message: %u \n ", ntohs(msg->type));  
#endif
      break;
  }
  return GNUNET_OK;
}


  GstBin *
get_app(GNUNET_gstData *d, int type)
{
  GstBin *bin;
  GstPad *pad, *ghostpad;

  if ( type == SOURCE )
  {
    bin = GST_BIN(gst_bin_new("Gnunet appsrc"));


    GNUNET_assert (GNUNET_OK ==
        GNUNET_log_setup ("gnunet-helper-audio-playback",
          "WARNING",
          NULL));

    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
        "Audio playback starts\n");

    GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
        "Audio playback starts\n");
#ifdef GGD
    fprintf(stderr," creating appsrc \n ");
#endif

    d->stdin_mst = GNUNET_SERVER_mst_create (&stdin_receiver, d);

    if ( d->stdin_mst == NULL)
    {
      fprintf(stderr,"stdin_mst = NULL"); 
      exit(10);
    }
    d->appsrc     = gst_element_factory_make ("appsrc",       "appsrc");

    gst_bin_add_many( bin, d->appsrc, NULL);
    //    gst_element_link_many ( encoder, muxer, NULL);

    pad = gst_element_get_static_pad (d->appsrc, "src");
    ghostpad = gst_ghost_pad_new ("src", pad);
  }
  if ( type == SINK )
  {
    bin = GST_BIN(gst_bin_new("Gnunet appsink"));


    GNUNET_assert (GNUNET_OK ==
        GNUNET_log_setup ("gnunet-helper-audio-record",
          "WARNING",
          NULL));

    GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
        "Audio source starts\n");

    d->appsink     = gst_element_factory_make ("appsink",       "appsink");

    // allocate memory for the audio messages
    d->audio_message = GNUNET_malloc (UINT16_MAX);
    // fill it with 7 for testing
    memset(d->audio_message, 7, 200);
    (d->audio_message)->header.type = htons (GNUNET_MESSAGE_TYPE_CONVERSATION_AUDIO);
#ifdef GGD
    fprintf(stderr,"headernumber number: %u \n",GNUNET_MESSAGE_TYPE_CONVERSATION_AUDIO);
#endif


    g_object_set (G_OBJECT (d->appsink), "emit-signals", TRUE, "sync", TRUE, NULL); 

    g_signal_connect (d->appsink, "new-sample",
        G_CALLBACK (on_appsink_new_sample), &d);

    gst_bin_add_many( bin, d->appsink, NULL);
    //    gst_element_link_many ( encoder, muxer, NULL);

    pad = gst_element_get_static_pad (d->appsink, "sink");
    ghostpad = gst_ghost_pad_new ("sink", pad);
  }

  /* set the bin pads */
  gst_pad_set_active (ghostpad, TRUE);
  gst_element_add_pad (GST_ELEMENT(bin), ghostpad);

  gst_object_unref (pad);

  return bin;
}

  extern GstBin *
get_coder(GNUNET_gstData *d , int type)
{
  GstBin *bin;
  GstPad *srcpad, *sinkpad, *srcghostpad, *sinkghostpad;
  GstCaps *caps, *rtpcaps;
  GstElement *encoder, *muxer, *decoder, *demuxer, *jitterbuffer, *rtpcapsfilter;

  if ( d->usertp == TRUE )
  {
    rtpcaps = gst_caps_new_simple ("application/x-rtp",
        "media", G_TYPE_STRING, "audio",
        "clock-rate", G_TYPE_INT, SAMPLING_RATE,
        "encoding-name", G_TYPE_STRING, "OPUS",
        "payload", G_TYPE_INT, 96,
        "sprop-stereo", G_TYPE_STRING, "0",
        "encoding-params", G_TYPE_STRING, "2",
        NULL); 

    rtpcapsfilter  = gst_element_factory_make ("capsfilter",    "rtpcapsfilter");

    g_object_set (G_OBJECT (rtpcapsfilter),
        "caps", rtpcaps,
        NULL);
    gst_caps_unref (rtpcaps);
  }


  if ( type == ENCODER )
  { 
    bin = GST_BIN(gst_bin_new("Gnunet audioencoder"));

    encoder  = gst_element_factory_make ("opusenc",       "opus-encoder");
    if ( d->usertp == TRUE )
    {
      muxer   = gst_element_factory_make ("rtpopuspay",        "rtp-payloader");
    } else {
      muxer   = gst_element_factory_make ("oggmux",        "ogg-muxer");
    }
    g_object_set (G_OBJECT (encoder),
        /*      "bitrate", 64000, */
        /*      "bandwidth", OPUS_BANDWIDTH_FULLBAND, */
        "inband-fec", INBAND_FEC_MODE,
        "packet-loss-percentage", PACKET_LOSS_PERCENTAGE,
        "max-payload-size", MAX_PAYLOAD_SIZE,
        "audio", TRUE, /* VoIP, not audio */
        "frame-size", OPUS_FRAME_SIZE,
        NULL);

    if ( d->usertp != TRUE)
    {
      g_object_set (G_OBJECT (muxer),
          "max-delay", OGG_MAX_DELAY,
          "max-page-delay", OGG_MAX_PAGE_DELAY,
          NULL);
    }

    gst_bin_add_many( bin, encoder, muxer, NULL);
    gst_element_link_many ( encoder, muxer, NULL);
    sinkpad = gst_element_get_static_pad(encoder, "sink");
    sinkghostpad = gst_ghost_pad_new ("sink", sinkpad);

    srcpad = gst_element_get_static_pad(muxer, "src");
    srcghostpad = gst_ghost_pad_new ("src", srcpad);

  }
  if ( type == DECODER ) 
  {
    bin = GST_BIN(gst_bin_new("Gnunet audiodecoder"));

    // decoder
    if ( d->usertp == TRUE )
    {

      demuxer  = gst_element_factory_make ("rtpopusdepay",      "ogg-demuxer");
      jitterbuffer = gst_element_factory_make ("rtpjitterbuffer", "rtpjitterbuffer");
    } else { 
      demuxer  = gst_element_factory_make ("oggdemux",      "ogg-demuxer");
    }
    decoder  = gst_element_factory_make ("opusdec",       "opus-decoder");

    if ( d->usertp == TRUE )
    {
      gst_bin_add_many( bin, rtpcapsfilter, jitterbuffer, demuxer, decoder, NULL);
      gst_element_link_many ( rtpcapsfilter, jitterbuffer, demuxer, decoder, NULL);
      sinkpad = gst_element_get_static_pad(rtpcapsfilter, "sink");

    } else {
      gst_bin_add_many( bin, demuxer, decoder, NULL);

      g_signal_connect (demuxer, 
          "pad-added",
          G_CALLBACK (decoder_ogg_pad_added), 
          decoder);

      sinkpad = gst_element_get_static_pad(demuxer, "sink");
    }
    sinkghostpad = gst_ghost_pad_new ("sink", sinkpad);

    srcpad = gst_element_get_static_pad(decoder, "src");
    srcghostpad = gst_ghost_pad_new ("src", srcpad);

  }

  // add pads to the bin
  gst_pad_set_active (sinkghostpad, TRUE);
  gst_element_add_pad (GST_ELEMENT(bin), sinkghostpad);

  gst_pad_set_active (srcghostpad, TRUE);
  gst_element_add_pad (GST_ELEMENT(bin), srcghostpad);


  return bin; 
}
  extern GstBin *
get_audiobin(GNUNET_gstData *d , int type)
{
  GstBin *bin;
  GstElement *sink, *source, *queue, *conv, *resampler, *removesilence, *filter;
  GstPad *pad, *ghostpad;
  GstCaps *caps;
  if ( type == SINK ) {

    bin = GST_BIN(gst_bin_new("Gnunet audiosink"));

    /* Create all the elements */
    if ( d->dropsilence == TRUE )
    {
      queue = gst_element_factory_make ("queue", "queue");
      removesilence = gst_element_factory_make ("removesilence", "removesilence");
    }

    conv     = gst_element_factory_make ("audioconvert",  "converter");
    resampler= gst_element_factory_make ("audioresample", "resampler");

    if ( d->audiobackend == AUTO )
    {
      sink     = gst_element_factory_make ("autoaudiosink", "audiosink");
      g_signal_connect (sink, "child-added", G_CALLBACK (autoaudiosink_child_added), NULL);

    }

    if ( d->audiobackend == ALSA )
    {
      sink     = gst_element_factory_make ("alsaaudiosink", "audiosink");
    }

    if ( d->audiobackend == JACK )
    {
      sink     = gst_element_factory_make ("jackaudiosink", "audiosink");

      g_object_set (G_OBJECT (sink), "client-name", "gnunet", NULL);

      if (g_object_class_find_property
          (G_OBJECT_GET_CLASS (sink), "port-pattern"))
      {

        g_object_set (G_OBJECT (sink), "port-pattern", d->jack_pp_out,
            NULL);
      }

    }

    if ( d->audiobackend == FAKE )
    {
      sink     = gst_element_factory_make ("fakesink", "audiosink");
    }

    g_object_set (sink,
        "buffer-time", (gint64) BUFFER_TIME, 
        "latency-time", (gint64) LATENCY_TIME,
        NULL);

    if ( d->dropsilence == TRUE )
    {
      // Do not remove silence by default
      g_object_set( removesilence, "remove", FALSE, NULL);
      g_object_set( queue, "max-size-buffers", 12,  NULL);
      /*
         g_signal_connect (source,
         "need-data",
         G_CALLBACK(appsrc_need_data),
         NULL);

         g_signal_connect (source,
         "enough-data",
         G_CALLBACK(appsrc_enough_data),
         NULL);
         */
      /*
         g_signal_connect (queue,
         "notify::current-level-bytes",
         G_CALLBACK(queue_current_level),
         NULL);

         g_signal_connect (queue,
         "underrun",
         G_CALLBACK(queue_underrun),
         NULL);

         g_signal_connect (queue,
         "running",
         G_CALLBACK(queue_running),
         NULL);

         g_signal_connect (queue,
         "overrun",
         G_CALLBACK(queue_overrun),
         NULL);

         g_signal_connect (queue,
         "pushing",
         G_CALLBACK(queue_pushing),
         NULL);
         */

    }

    gst_bin_add_many (bin ,  conv, resampler, sink, NULL);
    gst_element_link_many ( conv, resampler, sink, NULL);

    if ( d->dropsilence == TRUE )
    {
      gst_bin_add_many (bin , queue ,removesilence , NULL); 

      if ( !gst_element_link_many ( queue, removesilence, conv,  NULL) )
        lf ("queue, removesilence, conv ");

      pad = gst_element_get_static_pad (queue, "sink");

    } else {

      pad = gst_element_get_static_pad(conv, "sink");

    }

    ghostpad = gst_ghost_pad_new ("sink", pad);

  } else { 
    // SOURCE

    bin = GST_BIN(gst_bin_new("Gnunet audiosource"));

    //    source = gst_element_factory_make("audiotestsrc", "audiotestsrcbla");

    if (d->audiobackend == AUTO )
    {
      source     = gst_element_factory_make ("autoaudiosrc", "audiosource");
    }
    if (d->audiobackend == ALSA )
    {
      source     = gst_element_factory_make ("alsasrc", "audiosource");
    }
    if (d->audiobackend == JACK )
    {
      source     = gst_element_factory_make ("jackaudiosrc", "audiosource");
    }
    if (d->audiobackend == TEST )
    {
      source     = gst_element_factory_make ("audiotestsrc", "audiosource");
    }

    filter   = gst_element_factory_make ("capsfilter",    "filter");
    conv     = gst_element_factory_make ("audioconvert",  "converter");
    resampler= gst_element_factory_make ("audioresample", "resampler");

    if (d->audiobackend == AUTO ) {
      g_signal_connect (source, "child-added", G_CALLBACK (autoaudiosource_child_added), NULL);

    } else {
      if (GST_IS_AUDIO_BASE_SRC (source))
        g_object_set (source, "buffer-time", (gint64) BUFFER_TIME, "latency-time", (gint64) LATENCY_TIME, NULL);
      if ( d->audiobackend == JACK ) {
        g_object_set (G_OBJECT (source), "client-name", "gnunet", NULL);
        if (g_object_class_find_property
            (G_OBJECT_GET_CLASS (source), "port-pattern"))
        {

          char *portpattern = "moc";

          g_object_set (G_OBJECT (source), "port-pattern", portpattern,
              NULL);
        }
      }
    }

    caps = gst_caps_new_simple ("audio/x-raw",
        /*  "format", G_TYPE_STRING, "S16LE", */
        /*    "rate", G_TYPE_INT, SAMPLING_RATE,*/
        "channels", G_TYPE_INT, OPUS_CHANNELS,
        /*    "layout", G_TYPE_STRING, "interleaved",*/
        NULL);

    g_object_set (G_OBJECT (filter),
        "caps", caps,
        NULL);
    gst_caps_unref (caps);

    gst_bin_add_many (bin ,  source, filter, conv, resampler,  NULL);
    gst_element_link_many ( source, filter, conv, resampler, NULL);

    pad = gst_element_get_static_pad (resampler, "src");


    /* pads */
    ghostpad = gst_ghost_pad_new ("src", pad);

  }

  /* set the bin pads */
  gst_pad_set_active (ghostpad, TRUE);
  gst_element_add_pad (GST_ELEMENT(bin), ghostpad);

  gst_object_unref (pad);

  return bin;
}
